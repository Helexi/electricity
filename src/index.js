/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */

import { Household } from "./Household.js";
import { PowerPlant } from "./PowerPlanet.js";

export class World {
  constructor() {
    this.powerPlants = new Map();
    this.households = new Map();
  }

  createPowerPlant() {
    const plant = new PowerPlant();
    this.powerPlants.set(plant.id, plant);
    return plant.id;
  }

  createHousehold() {
    const household = new Household();
    this.households.set(household.id, household);
    return household.id;
  }

  connectHouseholdToPowerPlant(householdId, powerPlantId) {
    const powerPlant = this.powerPlants.get(powerPlantId);
    const coreHousehold = this.corecore(householdId);

    if (powerPlant.isWork) {
      coreHousehold.activePowerPlants++;
    }

    powerPlant.connectHousehold(householdId);
  }

  connectHouseholdToHousehold(household1, household2) {
    const coreHousehold1 = this.corecore(household1);
    const coreHousehold2 = this.corecore(household2);

    if (coreHousehold1.id !== coreHousehold2.id) {
      coreHousehold1.core = coreHousehold2.id;
      coreHousehold2.activePowerPlants += coreHousehold1.activePowerPlants;
    }
  }

  disconnectHouseholdFromPowerPlant(householdId, powerPlantId) {
    const powerPlant = this.powerPlants.get(powerPlantId);
    const coreHousehold = this.corecore(householdId);

    if (powerPlant.isWork) {
      coreHousehold.activePowerPlants--;
    }

    powerPlant.disconnectHousehold(householdId);
  }

  killPowerPlant(powerPlantId) {
    const powerPlant = this.powerPlants.get(powerPlantId);
    const connectedHouseholds = powerPlant.householdsConnect;

    if (powerPlant.isWork) {
      connectedHouseholds.forEach(householdId => {
        const coreHousehold = this.corecore(householdId);
        coreHousehold.activePowerPlants--;
      });

      powerPlant.isWork = false;
    }
  }

  repairPowerPlant(powerPlantId) {
    const powerPlant = this.powerPlants.get(powerPlantId);
    const connectedHouseholds = powerPlant.householdsConnect;

    if (!powerPlant.isWork) {
      connectedHouseholds.forEach(householdId => {
        const coreHousehold = this.corecore(householdId);
        coreHousehold.activePowerPlants++;
      });

      powerPlant.isWork = true;
    }
  }

  householdHasEletricity(householdId) {
    const coreHousehold = this.corecore(householdId);
    return coreHousehold.activePowerPlants > 0;
  }

  corecore(id) {
    let household = this.households.get(id);
    if (household.core !== id) {
      household = this.corecore(household.core);
    }
    household.core = household.id;
    return household;
  }
}
