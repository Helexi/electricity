export class PowerPlant {
  constructor() {
    this.id = Math.random();
    this.isWork = true;
    this.householdsConnect = new Set();
  }

  connectHousehold(householdId) {
    this.householdsConnect.add(householdId);
  }

  disconnectHousehold(householdId) {
    this.householdsConnect.delete(householdId);
  }
}