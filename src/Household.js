export class Household {
  constructor() {
    this.id = Math.random();
    this.core = this.id;
    this.powerPlantsConnect = new Set();
    this.householdsConnect = new Set();
    this.activePowerPlants = 0;
  }

  connectPowerPlant(powerPlantId) {
    this.powerPlantsConnect.add(powerPlantId);
  }

  disconnectPowerPlant(powerPlantId) {
    this.powerPlantsConnect.delete(powerPlantId);
  }

  connectHousehold(householdId) {
    this.householdsConnect.add(householdId);
  }

  disconnectHousehold(householdId) {
    this.householdsConnect.delete(householdId);
  }
}